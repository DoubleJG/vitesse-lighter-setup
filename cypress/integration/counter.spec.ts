context('Basic', () => {
  beforeEach(() => {
    cy.visit('/')
  })

  it ('Has the Counter.vue component', () => {
    cy.get('[data-test=\'counter\']').should('exist')
  })
})
