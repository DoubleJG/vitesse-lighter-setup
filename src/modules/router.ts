import { type UserModule } from '@/types'

export const install: UserModule = ({ router }) => {
  // Modify the router instance after routes in /pages are registered
  // Useful for adding things like global navigation guards
  router.beforeEach((to, _from) => {
    if (to.meta.requiresAuth) {
      const token = null
      if (!token) {
        return {
          path: '/login',
          query: { onto: to.fullPath },
        }
      }
    }
  })
}
